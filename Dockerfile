FROM ubuntu:20.04


RUN apt-get update
RUN apt-get install wget cron -y
RUN mkdir bitcoin
WORKDIR /bitcoin

ARG version

RUN wget -O 'bitcoinsv.tar.gz' https://download.bitcoinsv.io/bitcoinsv/${version}/bitcoin-sv-${version}-x86_64-linux-gnu.tar.gz
RUN tar -xvf bitcoinsv.tar.gz
RUN mv ./bitcoin-sv-${version} ./bitcoin-sv
COPY bitcoin.conf /bitcoin/bitcoin.conf
COPY start.sh .
COPY initialize.sh .

# cron
COPY functions.sh /
COPY close-block.sh .
COPY block-cron /etc/cron.d/block-cron
RUN crontab /etc/cron.d/block-cron

RUN mkdir data
EXPOSE 18332
EXPOSE 28332

CMD ["bash", "start.sh"]