# get blockchain height
HEIGHT=$(/bitcoin/bitcoin-sv/bin/bitcoin-cli -conf="/bitcoin/bitcoin.conf" getblockcount)

# initialize blockchain if needed
if [[ $HEIGHT -le "5" ]]
then
    source /functions.sh
    rpccommand generate 101
fi
