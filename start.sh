#!/bin/bash

cron # start cron to create new blocks

(sleep 5 && bash /bitcoin/initialize.sh) & # initialize blockchain after 5 seconds

if [[ -z $RPCPASSWORD ]]
then
    ./bitcoin-sv/bin/bitcoind -datadir="/bitcoin/data" -conf="/bitcoin/bitcoin.conf"
else
    ./bitcoin-sv/bin/bitcoind -datadir="/bitcoin/data" -conf="/bitcoin/bitcoin.conf" -rpcuser=$RPCUSER -rpcpassword=$RPCPASSWORD
fi
echo el password es $RPCPASSWORD