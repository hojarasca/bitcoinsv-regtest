#!/bin/bash

source constants.sh

docker build \
    --build-arg version=$VERSION \
    -t hojarasca/bitcoinsv-regtest:$VERSION \
    .