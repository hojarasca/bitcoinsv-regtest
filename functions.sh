function rpccommand {
    if [[ -z $RPCPASSWORD ]]
    then
        /bitcoin/bitcoin-sv/bin/bitcoin-cli -conf="/bitcoin/bitcoin.conf" $@
    else
        /bitcoin/bitcoin-sv/bin/bitcoin-cli -conf="/bitcoin/bitcoin.conf" -rpcuser=$RPCUSER -rpcpassword=$RPCPASSWORD $@
    fi
}
