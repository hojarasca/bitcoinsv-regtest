# Bitcoin sv regtest image

This is a docker image meant to easily develop against a bitcoin-sv regtest node.

It includes a few things that are useful and easy to forget when setting a dev envinronment
from scratch:

- Automatic block close every 10 minutes.
- Development configurations already set.
- All the consensoues parameters set to standard values.
- Genesis set.
- Initializes blockchain automatically.
- Node wallet full of bitcoin ready to send around.
- Easy to set up with docker-compose.
- rpc and zmq enabled.

## Usage with docker-compose

``` yml
version: '2'
services:
  bitcoin:
    image: hojarasca/bitcoinsv-regtest:1.0.8.beta
    ports:
    - 18332:18332 # rpc
    - 18333:18333 # p2p
    - 28332:28332 # zmq
    volumes:
    - bsvnode-data:/bitcoin/data
    environment:
      RPCUSER: rpcuser
      RPCPASSWORD: sosecret

  bitcoin-rpc-consumer:
    build: .
    environment:
      RPC_URL: 'http://rpcuser:sosecret@bitcoin:18332' # uri to connect to rpc

  bitcoin-zmq-consumer:
    build: .
    environment:
      ZMQ_PUB_ADDRESS: 'tcp://bitcoin:28332' # uri to connect to zmq

volumes: 
    bsvnode-data:
```


## Build

``` bash
bash build.sh
```
## Publish

``` bash
bash publish.sh
```